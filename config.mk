# See COPYING file for copyright and license details.
VERSION = 1.2
RELDATE = 2015-07-28

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man
OPENSSL = /home/nick/tmp/openssl-1.1.1m

LIBS = -lssl -lcrypto

CFLAGS = -std=c99 -pedantic -Wall -Wextra -g -D_POSIX_C_SOURCE=200112L \
         -DVERSION=\"$(VERSION)\" -I$(OPENSSL)/include

# glibc dynamic
CC = cc
LDFLAGS = $(LIBS)

# musl static
#CC = musl-gcc -static
#LDFLAGS = $(LIBS) -L$(OPENSSL) -static #-s

# mingw
#W32TCLKIT = win-build-deps/tclkit-gui-8_6_11-twapi-4_5_2-x86-max.exe
#OPENSSLDIR = win-build-deps/openssl-1.1.1l
#SDX = ./win-build-deps/sdx
#CC = i686-w64-mingw32-gcc
#AR = i686-w64-mingw32-ar
#CFLAGS = -ansi -Wall -DVERSION=\"$(VERSION)\" -DWINVER=0x0501 -I$(OPENSSLDIR)/include
#LDFLAGS = -L$(OPENSSLDIR) $(LIBS) -lws2_32 -static

LD = $(CC)
