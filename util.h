/* See COPYING file for copyright and license details. */
#define COOKIEMAX 1024
#include <openssl/ssl.h>
#include <openssl/err.h>
typedef struct {
        int fd;
        SSL *sslhandle;
} conn;
conn *dial(char *host, char *port, int ssl);
int get(char *host, int ssl, char *path, char *sendcookie, char *savecookie, char **body, int istext);
int post(char *host, int ssl, char *path, char *sendcookie, char *savecookie, char *data, char **body, int istext);
int gettofile(char *host, int ssl, char *url, char *sendcookie, char *savecookie, char *savepath, int istext);
int renameifjpg(char *path);
